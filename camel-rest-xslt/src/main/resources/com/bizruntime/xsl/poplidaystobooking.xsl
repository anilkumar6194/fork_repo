<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<OTA_HotelAvailNotifRQ xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns="http://www.opentravel.org/OTA/2003/05"
			xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelAvailNotifRQ.xsd"
			TimeStamp="2012-10-05T14:20:50" Target="Test" Version="1.005">
			<AvailStatusMessages>
				<AvailStatusMessage LocatorID="2">
					<StatusApplicationControl Start="2012-11-05"
						End="2012-11-17" InvTypeCode="36745603" RatePlanCode="1278606" />
					<LengthsOfStay>
						<LengthOfStay Time="2" MinMaxMessageType="SetMinLOS" />
						<LengthOfStay Time="5" MinMaxMessageType="SetMaxLOS" />
					</LengthsOfStay>
					<RestrictionStatus Status="Open" />
				</AvailStatusMessage>
			</AvailStatusMessages>
		</OTA_HotelAvailNotifRQ>
	</xsl:template>
</xsl:stylesheet>